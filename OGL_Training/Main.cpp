/*
	File: Main.cpp
	Author: Thomas Gredin
	Description: Program to train to OpenGL and game logic

	//// Clicker game type ??
*/

#include "Game.h"

int main()
{
	Game* game = new Game(1280, 720, "Clicker");
	game->start();

	return 0;
}