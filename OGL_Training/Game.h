#pragma once

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <iostream>

#include "Graphics/Sprite.h"
#include "Graphics/OrthoCamera.h"

class Game
{
private:
	GLFWwindow* m_win;

	ShaderProgram* shader;
	Sprite* test;
	OrthoCamera* camera;

public:
	Game(int width, int height, const char* title);
	~Game();

	void init();
	void update();
	void render();

	void start();
};

/*
	TODO :
	- Add model matrix to sprite (Maybe use Transform class)
	- Load texture for sprite
*/