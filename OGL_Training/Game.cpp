#include "Game.h"

Game::Game(int width, int height, const char* title)
{
	/* Init contexte */
	int init_state = glfwInit();
	if(!init_state)
	{
		std::cerr << "[GAME] Error while init GLFW and OpenGL context" << std::endl;
		exit(EXIT_FAILURE);
	}

	/* Tweak context's settings */
	glfwWindowHint(GLFW_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_VERSION_MINOR, 3);

	m_win = glfwCreateWindow(width, height, title, NULL, NULL);

	if(!m_win)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	/* Make created window as active context */
	glfwMakeContextCurrent(m_win);

	/* Init glew to load GL functions */
	glewExperimental = GL_TRUE;
	GLenum glew_init_state = glewInit();
	if(glew_init_state != GLEW_OK)
	{
		std::cerr << "[GAME] Error while init GLEW" << std::endl;
		exit(EXIT_FAILURE);
	}
}

Game::~Game()
{
	delete shader;
	delete test;
	delete camera;
	glfwTerminate();
}

void Game::init()
{
	shader = new ShaderProgram("assets/vertex.glsl", "assets/fragment.glsl");
	test = new Sprite("assets/stone.png");
	camera = new OrthoCamera();
}

void Game::update()
{

}

void Game::render()
{
	shader->use();

	test->render(shader, camera);

	shader->done();
}

void Game::start()
{
	glfwSwapInterval(1);
	init();

	while(!glfwWindowShouldClose(m_win))
	{
		glfwPollEvents();
		update();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		render();

		glfwSwapBuffers(m_win);
	}
}
