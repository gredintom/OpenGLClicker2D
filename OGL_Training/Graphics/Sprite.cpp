#include "Sprite.h"

Sprite::Sprite(const char* tex)
{
	GLfloat data[] = {
		-0.5f, 0.5f, 0,
		0.5f, 0.5f, 0,
		-0.5f, -0.5f, 0,

		0.5f, 0.5f, 0,
		0.5f, -0.5f, 0,
		-0.5f, -0.5f, 0,
	};

	GLfloat uvs[] = {
		0.f, 0.f,
		1.f, 0.f,
		0.f, 1.f,

		1.f, 0.f,
		1.f, 1.f,
		0.f, 1.f,
	};

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	/* VBO */
	glEnableVertexAttribArray(0);
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(float), data, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(1);
	glGenBuffers(1, &m_uvs);
	glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), uvs, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	m_texture.loadFromFile(tex);

	m_transform.setScales(m_texture.getWidth(), m_texture.getHeight(), 0.f);
	m_transform.setPosition(200, 200, 0);
}

Sprite::~Sprite()
{
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_uvs);
	glDeleteVertexArrays(1, &m_vao);
}

void Sprite::render(ShaderProgram * shader, OrthoCamera* camera)
{
	m_transform.setRotations(0.f, 0.f, m_transform.getRotations().z + 0.01f);

	m_texture.bind();
	shader->setUniformInt("tex", m_texture.getUnit());
	shader->setUniformMat4("mvp", camera->getMatrix() * m_transform.getMatrix());

	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

	m_texture.unbind();
}
