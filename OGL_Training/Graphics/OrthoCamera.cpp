#include "OrthoCamera.h"

OrthoCamera::OrthoCamera()
{
}

OrthoCamera::~OrthoCamera()
{
}

glm::mat4 OrthoCamera::getMatrix()
{
	m_projection = glm::ortho(0.f, 854.f, 480.f, 0.f, -5.0f, 5.0f);
	m_view = glm::lookAt(glm::vec3(0,0,5), glm::vec3(0,0,0), glm::vec3(0,1,0));

	return m_projection * m_view;
}
