#pragma once

#include <glm\gtc\matrix_transform.hpp>

class Transform
{
private:
	glm::vec3 m_position;
	glm::vec3 m_scales;
	glm::vec3 m_rotations;

public:
	Transform();
	~Transform();

	// Getters

	glm::vec3 getPosition();
	glm::vec3 getScales();
	glm::vec3 getRotations();

	glm::mat4 getMatrix();

	// Setters

	void setPosition(glm::vec3 position);
	void setPosition(float x, float y, float z);
	void move(glm::vec3 offset);
	void move(float x, float y, float z);
	void setScales(glm::vec3 scales);
	void setScales(float x, float y, float z);
	void setRotations(glm::vec3 rotations);
	void setRotations(float x, float y, float z);

};