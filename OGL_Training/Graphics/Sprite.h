#pragma once

#include "ShaderProgram.h"
#include "OrthoCamera.h"
#include "Texture.h"
#include "Transform.h"

class Sprite
{
private:
	GLuint m_vao;
	GLuint m_vbo;
	GLuint m_uvs;

	Transform m_transform;
	Texture m_texture;

public:
	Sprite(const char* tex);
	~Sprite();

	void render(ShaderProgram* shader, OrthoCamera* camera);
};

