#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

Texture::Texture(GLuint unit)
{
	m_unit = unit;
	m_id = NO_GEN_TEX;
}

Texture::Texture(const char * file, GLuint unit)
{
	m_unit = unit;
	m_id = NO_GEN_TEX;

	loadFromFile(file);
}

Texture::~Texture()
{
	glDeleteTextures(1, &m_id);
}

bool Texture::loadFromFile(const char * file)
{
	if (m_id != NO_GEN_TEX)
	{
		glDeleteTextures(1, &m_id);
	}

	glGenTextures(1, &m_id);
	bind();

	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/*Load image from file*/
	int width, height, channels;

	/*Load data from file*/
	unsigned char* data = stbi_load(file, &width, &height, &channels, 0);

	m_width = width;
	m_height = height;

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	stbi_image_free(data);

	unbind();

	return true;
}

void Texture::bind()
{
	glActiveTexture(GL_TEXTURE0 + m_unit);
	glBindTexture(GL_TEXTURE_2D, m_id);
}

void Texture::unbind()
{
	glBindTexture(GL_TEXTURE_2D, NO_GEN_TEX);
}

GLuint Texture::getUnit()
{
	return m_unit;
}

GLuint Texture::getWidth()
{
	return m_width;
}

GLuint Texture::getHeight()
{
	return m_height;
}
