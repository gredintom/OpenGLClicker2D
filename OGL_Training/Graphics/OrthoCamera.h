#pragma once

#include <glm/gtc/matrix_transform.hpp>

class OrthoCamera
{
private:
	glm::vec3 m_position;
	glm::mat4 m_projection, m_view;

public:
	OrthoCamera();
	~OrthoCamera();

	glm::mat4 getMatrix();
};

