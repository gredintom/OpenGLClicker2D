#include "ShaderProgram.h"

ShaderProgram::ShaderProgram(const char* vertex, const char* fragment)
{
	// Load vertex Shader content
	std::ifstream v_stream, f_stream;
	v_stream.open(vertex);
	f_stream.open(fragment);

	if(!v_stream.is_open())
	{
		std::cerr << "[SHADER PROGRAM] Can't open Vertex shader source file" << std::endl;
		exit(EXIT_FAILURE);
	}
	
	if(!f_stream.is_open())
	{
		std::cerr << "[SHADER PROGRAM] Can't open Fragment shader source file" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::string v_content, f_content;

	/* Load vertex shader content */
	std::stringstream v_sstream;
	v_sstream << v_stream.rdbuf();
	v_content = v_sstream.str();
	v_stream.close();

	/* Load fragment shader content */
	std::stringstream f_sstream;
	f_sstream << f_stream.rdbuf();
	f_content = f_sstream.str();
	f_stream.close();

	/* Create shaders */
	GLuint v_shader, f_shader;

	/* Vertex shader */
	v_shader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* v_char_content = v_content.c_str();
	glShaderSource(v_shader, 1, &v_char_content, NULL);
	glCompileShader(v_shader);

	GLint Result = GL_FALSE;
	int InfoLogLength;

	glGetShaderiv(v_shader, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(v_shader, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 )
	{
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(v_shader, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		std::cerr << &VertexShaderErrorMessage[0] << std::endl;
	}

	/* Fragment shader */
	f_shader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* f_char_content = f_content.c_str();
	glShaderSource(f_shader, 1, &f_char_content, NULL);
	glCompileShader(f_shader);

	glGetShaderiv(f_shader, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(f_shader, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 )
	{
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(f_shader, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		std::cerr << &FragmentShaderErrorMessage[0] << std::endl;
	}

	/* Create program */
	m_program_id = glCreateProgram();
	glAttachShader(m_program_id, v_shader);
	glAttachShader(m_program_id, f_shader);
	glLinkProgram(m_program_id);

	glGetProgramiv(m_program_id, GL_LINK_STATUS, &Result);
	glGetProgramiv(m_program_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(m_program_id, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		std::cerr << &ProgramErrorMessage[0] << std::endl;
	}

	glDetachShader(m_program_id, v_shader);
	glDetachShader(m_program_id, f_shader);

	glDeleteShader(v_shader);
	glDeleteShader(f_shader);
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(m_program_id);
}

void ShaderProgram::use()
{
	glUseProgram(m_program_id);
}

void ShaderProgram::done()
{
	glUseProgram(0);
}

void ShaderProgram::setUniformMat4(const char * name, glm::mat4 mat)
{
	GLuint location = glGetUniformLocation(m_program_id, name);
	if(location == -1)
	{
		std::cerr << "[SHADER PROGRAM] No location found for name \"" << name << "\"" << std::endl;
		return;
	}

	glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]);
}

void ShaderProgram::setUniformInt(const char * name, GLuint val)
{
	GLuint location = glGetUniformLocation(m_program_id, name);
	if(location == -1)
	{
		std::cerr << "[SHADER PROGRAM] No location found for name \"" << name << "\"" << std::endl;
		return;
	}

	glUniform1i(location, val);
}
