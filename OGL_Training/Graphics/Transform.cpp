#include "Transform.h"

Transform::Transform()
{
	m_position = glm::vec3(0.f, 0.f, 0.f);
	m_scales = glm::vec3(1.f, 1.f, 1.f);
	m_rotations = glm::vec3(0.f, 0.f, 0.f);
}

Transform::~Transform()
{
}

glm::vec3 Transform::getPosition()
{
	return m_position;
}

glm::vec3 Transform::getScales()
{
	return m_scales;
}

glm::vec3 Transform::getRotations()
{
	return m_rotations;
}

glm::mat4 Transform::getMatrix()
{
	glm::mat4 model(1.f);
	model = glm::translate(model, m_position) *
		glm::rotate(model, m_rotations.x, glm::vec3(1, 0, 0)) *
		glm::rotate(model, m_rotations.y, glm::vec3(0, 1, 0)) *
		glm::rotate(model, m_rotations.z, glm::vec3(0, 0, 1)) *
		glm::scale(model, m_scales);

	return model;
}

void Transform::setPosition(glm::vec3 position)
{
	m_position = position;
}

void Transform::setPosition(float x, float y, float z)
{
	m_position = glm::vec3(x, y, z);
}

void Transform::move(glm::vec3 offset)
{
	m_position += offset;
}

void Transform::move(float x, float y, float z)
{
	m_position += glm::vec3(x, y, z);
}

void Transform::setScales(glm::vec3 scales)
{
	m_scales = scales;
}

void Transform::setScales(float x, float y, float z)
{
	m_scales = glm::vec3(x, y, z);
}

void Transform::setRotations(glm::vec3 rotations)
{
	m_rotations = rotations;
}

void Transform::setRotations(float x, float y, float z)
{
	m_rotations = glm::vec3(x, y, z);
}
