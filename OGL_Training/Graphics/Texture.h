#pragma once

#include <GL\glew.h>

class Texture
{
private:
	GLuint m_id;
	GLuint m_unit;

	GLuint m_width, m_height;

public:
	Texture(GLuint unit = 0);
	Texture(const char* file, GLuint unit = 0);
	~Texture();

	bool loadFromFile(const char* file);

	void bind();
	void unbind();
	GLuint getUnit();

	// Getters

	GLuint getWidth();
	GLuint getHeight();

	// Constants

	const static GLuint NO_GEN_TEX = 0;
};