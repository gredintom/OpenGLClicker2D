#pragma once

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

class ShaderProgram
{
private:
	GLuint m_program_id;

public:
	ShaderProgram(const char* vertex, const char* fragment);
	~ShaderProgram();

	void use();
	void done();

	void setUniformMat4(const char* name, glm::mat4 mat);
	void setUniformInt(const char* name, GLuint val);
};

